# Adaptive Feedback on LSM880

## Rough Outline

### ZEN

In ZEN Black open the PipelineConstructor.lvb macro (via Macro Menu); it is saved in C:\ZEN\Macros

#### For each imaging job save an image

Typically you would have one imaging job that does a low-resolution prescan to find the position of your object of interest and another job that acquires a high-res (e.g. SR AiryScan) image. 

#### Load saved images as jobs into PipelineConstructor workflow 

...

#### Configure the PipelineConstructor workflow

You need to specicy the sequence and which image will be used by your analysis software (method: online image analysis).

#### Specify the directory where the files are saved

For large data sets such as the AiryScan images it is important that you use the **TURBO** drive (E:)


### Image Analysis Software

#### Monitor whether specific files appear in a specified folder

The filenames are: prefix_DE_jobID_Well_Position_Timepoint.czi (or .lsm). 

For example:

MyImage_DE_1_* would be your pre-scan that you want to analyze.
MyImage_DE_2_* would be your real data set, acquired at the position determined by analyzing above image.

#### Open the file

Your analysis software needs to load the file, e.g. *_DE_1_*.czi. You need the bio-formats library to open the czi files; there are python and java bindings to this library.

#### Analyze the file 

For instance, determine the center of mass.

#### Send back new coordinates or error message to microscope

The commands are send via the WindowsRegistry.
You can send new X,Y,Z positions as well as an imaging ROI

## Example code for Fiji
 
Here is an example Fiji jython code that enables you to manually click on the new position:
https://git.embl.de/grp-almf/adaptive-feedback-lsm880-imagej-manual-clicking/blob/master/fiji--adaptive-feedback-ZenBlack--manual-relocation.py

This file needs some dependencies:

- AutoMicTools: https://git.embl.de/halavaty/AutoFRAP_JavaTools; it is used to write to registry (from automic.online.jobdistributors import WindowsRegistry)

- Microscope communicator: https://github.com/cmci/CommMicroscope; from this place embl.almf dependencies are taken.

