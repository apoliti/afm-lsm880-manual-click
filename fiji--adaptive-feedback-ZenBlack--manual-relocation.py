# Imports from MicroscopeCommunicator
from embl.almf import RunJythonOnMonitoredFiles # basic class for MyMonitor, implements monitoring functionality on selected folder, triggers processing function when new file apears
from embl.almf import Close_all_forced # to close all opened images

# basic ImageJ classes
from ij import IJ, ImageJ

# IJ GUI classes
from ij.gui import WaitForUserDialog
from ij.gui import GenericDialog
from ij.io import DirectoryChooser

# to open .czi and .lsm files
from loci.plugins import BF

# These two imports are needed to openimage in the predefined location on the screen 
from ij.gui import ImageWindow
from java.awt import Toolkit


from os import path
import sys

from automic.online.jobdistributors import WindowsRegistry	
# this imported class writes instances to Windows Registry
# implementation will likely depend on the system
# refer to https://git.embl.de/halavaty/AutoFRAP_JavaTools/blob/master/src/main/java/automic/online/jobdistributors/WindowsRegistry.java
# too see how it is currently implemented

defalutWatchPath='C:\\tempDat\\AutoFRAP_test'
imageFileNameTag='_DE_1_'
fileExtension='.czi'

def getSelectedPoint(_image):
    '''
    This function identifies point selection on the image and returns its coordinates (x,y,z).
    1. checks if the Image is Opened.
    2. Waits intil point is selected and confirmed
    3. Processes and generates output.
    Last output defines whether point selection was found
    '''
    
    if not _image.isVisible():
        return None,None,None,False
    
    _image.setActivated()
    IJ.setTool("multipoint")
    
    WaitForUserDialog("Select Point").show()
    selectedRoi=_image.getRoi()
    selectedZPos=_image.getSlice()-1
        
    if selectedRoi is None:
        return None,None,None,False

    pointPolygon=selectedRoi.getPolygon()
    
    return pointPolygon.xpoints[0],pointPolygon.ypoints[0],selectedZPos,True



# predefined constants for writing to registry in order to communicate with ZEN
# Refer to "Show OIA Keys" in Pipeline Constructor macro	
## registry location
reg_key = "HKCU\\SOFTWARE\\VB and VBA Program Settings\\OnlineImageAnalysis\\macro" 
## registry keys
subkey_codemic = "codeMic"
subkey_codeoia = "codeOia"
subkey_filename = "filepath"
subkey_xpos = "X"
subkey_ypos = "Y"
subkey_zpos = "Z"
subkey_fcsx = "fcsX"
subkey_fcsy = "fcsY"
subkey_fcsz = "fcsZ"
subkey_roix = "roiX"
subkey_roiy = "roiY"
subkey_roitype = "roiType"
subkey_roiaim = "roiAim"

subkey_errormsg = "errorMsg"

def submitCommandsToMicroscope(_codeM='', _X='', _Y='', _Z='', _roiT='', _roiA='', _roiX='', _roiY='', _errMsg=''):
	'''
	function that submits commands to microscope by writing them to Windows registry
	all arguments should be strings
	'''
		
	#submit new position in image coordinates of the Low-Zoom image
	if _X is not None: WindowsRegistry.writeRegistry(reg_key, subkey_xpos, _X)
	if _Y is not None: WindowsRegistry.writeRegistry(reg_key, subkey_ypos, _Y)
	if _Z is not None: WindowsRegistry.writeRegistry(reg_key, subkey_zpos, _Z)
		
	#define one or more Rois if needed. Can be used for imaging selected regions or for photomanipulation
	if _roiT is not None: WindowsRegistry.writeRegistry(reg_key, subkey_roitype,_roiT)
	if _roiA is not None: WindowsRegistry.writeRegistry(reg_key, subkey_roiaim ,_roiA)
	if _roiX is not None: WindowsRegistry.writeRegistry(reg_key, subkey_roix,_roiX)
	if _roiY is not None: WindowsRegistry.writeRegistry(reg_key, subkey_roiy,_roiY)
		
	#error message if someting goes wrong. Pipeline constructor copies this message to its log file
	if _errMsg is not None: WindowsRegistry.writeRegistry(reg_key, subkey_errormsg, _errMsg)
		
	#message to microscope what to do. Should be the last thing written to registry as it triggers imaging.
	if _codeM is not None: WindowsRegistry.writeRegistry(reg_key, subkey_codemic, _codeM)



class MyMonitor (RunJythonOnMonitoredFiles):
    #main class that monitors experimental folder, calls required image processing jobs and sends fedback to microscope.
    
    
    def __init__(self,_demo,_analchanges):#, _demo=False
        RunJythonOnMonitoredFiles.__init__(self)
        self.analyseChangedFiles=_analchanges
        self.demo=_demo
        self.exceptionCount=0

    def RunDistributor(self,fileAbsolutePath,_demo=True):
        IJ.freeMemory()
        IJ.log ('Current memory is %i' %IJ.currentMemory())
        Close_all_forced().run('')#IJ.run('Close all forced','')
        
        
        
        image=BF.openImagePlus(fileAbsolutePath)[0]
        if _demo:
            swd=Toolkit.getDefaultToolkit().getScreenSize().width	###get actual width of the screen
            ImageWindow.setNextLocation(swd+100,120)		#will push image to the second screen. For one screen use #swd-600# as first agrgument
            image.show()
        
        x,y,z,analysisSuccess=getSelectedPoint(image)
        
        if(analysisSuccess is True):    
            submitCommandsToMicroscope(_codeM='focus', _X=str(x),_Y=str(y), _Z=str(z))
        else:
           submitCommandsToMicroscope(_codeM='nothing', _errMsg='Suitable position is not found')
            
    
    def runOnNewFile(self, *args):
        try: #In case something goes wrong monitor does not have to crash. Current acquisition will not be recorded in summary file
            fileAbsolutePath = args[0].getCanonicalPath()
            IJ.log('New file: %s' %fileAbsolutePath)
            
            fileName=path.basename(fileAbsolutePath)
            
            if (path.exists(fileAbsolutePath) and fileName.endswith(fileExtension) and (fileName.find(imageFileNameTag)>=0)):
                    self.RunDistributor(fileAbsolutePath,self.demo)
        except:
            IJ.log('Exception rased')
            self.exceptionCount+=1
            submitCommandsToMicroscope(_codeM='nothing', _errMsg='Exception raised in online image analysis')

    def runOnChangedFile(self, *args):
        if self.analyseChangedFiles:
            runOnNewFile(self, *args)
        else:
            pass

    def runOnFileRemove(self, *args):
        pass

def initDebugConsole():
    ImageJ()
    demo=True
    analyseChangedFiles=False
    
    
    return defalutWatchPath,demo,analyseChangedFiles
    
def initFijiPluginWithDialogs():
    analyseChangedFiles=False
    demo=True
    #startup menu
    start_menu=GenericDialog('Monitoring startup')
    start_menu.addCheckbox('Show demo',demo)
    start_menu.addCheckbox('Analyse changed files',analyseChangedFiles)
    start_menu.showDialog();
    
    if start_menu.wasCanceled():
        sys.exit()
    demo=start_menu.getNextBoolean()
    analyseChangedFiles=start_menu.getNextBoolean()

    
    dc=DirectoryChooser('Select experiment directory')
    analysisPath=dc.getDirectory()
    if analysisPath is None:
        IJ.showMessage('Processing was aborted')
        return None
    DirectoryChooser.setDefaultDirectory(analysisPath)

    return analysisPath,demo,analyseChangedFiles

if __name__ == '__main__':
    if IJ.getInstance() is None:
        analysisPath,demo,analyseChangedFiles=initDebugConsole()
    else:
        analysisPath,demo,analyseChangedFiles=initFijiPluginWithDialogs()
    
    #Start monitoring
    IJ.log('Start monitoring')
    monitor=MyMonitor(_demo=demo,_analchanges=analyseChangedFiles)#_demo=True
    monitor.setWatchpath(analysisPath)
    try:
        monitor.startMonitoring()
    except:
        print 'Unable to start monitoring'

    WaitForUserDialog('File Monitor','Press OK to Stop Monitor').show()
    
    monitor.stopMonitor()
    IJ.log('Stop monitoring')
    IJ.log('%d exceptions were raised' %monitor.exceptionCount)